import {Component, OnInit} from '@angular/core';
import {Table} from './Table';

import {ActivitiesService} from '../../../services/activities.service';
@Component({
    selector: 'app-lesson2',
    templateUrl: './lesson2.component.html',
    styleUrls: ['./lesson2.component.scss']
})
export class Lesson2Component implements OnInit {
      tablerJson: Table;
      uploaded;
      success = true;
      shadow;

    total = 0;


    constructor(private activity: ActivitiesService) {
        this.tablerJson = new Table('', '', '', '', '', '', '', '', '', '',);

    }

    answ() {
        // alert(JSON.stringify(this.tablerJson.s1))
        this.uploaded = !this.uploaded;
        let sid = JSON.parse(localStorage.getItem('student'))['username'];
        let marks;


        this.activity.saveActivity2(sid, this.tablerJson.k1, this.tablerJson.k2, this.tablerJson.l1, this.tablerJson.l2,
            this.tablerJson.n1, this.tablerJson.n2, this.tablerJson.s1, this.tablerJson.s2, this.tablerJson.t1, this.tablerJson.t2).subscribe(res => {
            this.success = false;
        })
        alert(this.total);
           this.activity.saveMarksForActivity2(sid, this.total, 'AutoMated').subscribe(
         res => {

         }
         );

    }

    ngOnInit() {
    }

    allowDrop(ev) {
        ev.preventDefault();
    }

    drag(ev) {
        ev.dataTransfer.setData("Text", ev.target.id);
    }

    drop(ev) {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("Text");
        ev.target.parentNode.replaceChild(document.getElementById(data), ev.target);
        document.getElementById(data).className = "";
        this.filterData(ev.target.id, document.getElementById(data).innerText)
        // alert(ev.target.id+'  '+document.getElementById(data).innerText);
        this.shadow = JSON.stringify(this.tablerJson)
    }

    filterData(id, data: string) {
        // silent s > island > aisle
        //silent l > should > could
        //silent k > knock >
        //silent n > autumn > column
        //silent t > bustle > estle

        switch (id) {
            case 'div1':
                this.tablerJson.s1 = data;
                this.checkNow(data, 'Island', 'aisle')
                return;
            case 'div2':
                this.tablerJson.l1 = data;
                this.checkNow(data, 'could', 'should')
                return;
            case 'div3':
                this.tablerJson.k1 = data;
                this.checkNow(data, 'knock', ' ')
                return;
            case 'div4':
                this.tablerJson.n1 = data;
                this.checkNow(data, 'coulmn', 'autmn')
                return;
            case 'div5':
                this.tablerJson.t1 = data;
                this.checkNow(data, 'bustle', 'estle')
                return;

            case 'div6':
                this.tablerJson.s2 = data;
                this.checkNow(data, 'Island', 'aisle')
                return;
            case 'div7':
                this.tablerJson.l2 = data;
                this.checkNow(data, 'could', 'should')
                return;
            case 'div8':
                this.tablerJson.k2 = data;
                this.checkNow(data, 'knock', ' ')
                return;
            case 'div9':
                this.tablerJson.n2 = data;
                this.checkNow(data, 'coulmn', 'autmn')
                return;
            case 'div10':
                this.tablerJson.t2 = data;
                this.checkNow(data, 'bustle', 'estle')
                return;
        }
    }

    checkNow(answer, correctAnswer1, correctAnswer2) {
        if (answer === correctAnswer1 || answer === correctAnswer2) {
            this.total += 20;

        }

    }


}
