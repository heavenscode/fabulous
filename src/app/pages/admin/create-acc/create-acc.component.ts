import { Component, OnInit } from '@angular/core';
import { Signup } from '../../../dataModel/signup.model';
import { FormsModule } from '@angular/forms';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { RegisterStudent } from '../../../services/registerstudent.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-create-acc',
  templateUrl: './create-acc.component.html',
  styleUrls: ['./create-acc.component.scss']
})
export class CreateAccComponent implements OnInit {
  userForm: FormGroup;
  existingUsername = 'username already taken';
  mismatchpass = false;
  pwdPattern = '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,12}$';
  unamePattern = '^[a-z0-9_-]{8,15}$';
  errorShow = !true;
  signup: Signup;
  signupShadow;
  pwdLength = '100%';
  danger = false;
  warning = false;
  info = false;
  isok = false;
  wrongpassword=false;
  constructor(private formBuilder: FormBuilder, private regStd: RegisterStudent, private toastr: ToastrService) {
    this.signup = new Signup('', '', '', '', '', '', '', '');

  }


  ngOnInit() {


  }
  foramSubmit() {
    this.regStd.studentReg(this.signup.username,
      this.signup.password, this.signup.school, this.signup.sid,
      this.signup.firstname, this.signup.lastname, this.signup.dob).subscribe(data => {
        this.toastr.success('New Record Added!', 'Success!');

        return true;
      },
        error => {
          this.toastr.error('Error Occured', 'Alert!');
          return false;

        }
      );


  }

  checkuname;
  signupListner() {
    this.signupShadow = JSON.stringify(this.signup);

    this.regStd.checkusername(this.signup.username).subscribe(
      res => {
        this.checkuname = Number(String(res.json()['count']));
        console.log(this.checkuname);
        if (this.checkuname === 1) {
          console.log('username already taken ' + this.checkuname);
          this.errorShow = true;
        } else {
          this.errorShow = !true;
          console.log('username available ' + this.checkuname);
        }
      }

    );
if (this.signup.password!==this.signup.retypePassword){
  this.wrongpassword=true;

}else{
  this.wrongpassword=!true;
}
    //console.log(this.signup.password.length);
    let pwdpercent = (this.signup.password.length / 8) * 100;
    this.pwdLength = String(pwdpercent) + '%';
    if (pwdpercent <= 34) {
      this.danger = true;
      this.warning = false;
      this.info = false;
    } else if (pwdpercent <= 60 && pwdpercent > 34) {
      this.danger = false;
      this.warning = !false;
      this.info = !true;

    } else {
      this.danger = false;
      this.warning = false;
      this.info = true;

    }
  }

}