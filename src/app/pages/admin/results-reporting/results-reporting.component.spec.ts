import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsReportingComponent } from './results-reporting.component';

describe('ResultsReportingComponent', () => {
  let component: ResultsReportingComponent;
  let fixture: ComponentFixture<ResultsReportingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsReportingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsReportingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
