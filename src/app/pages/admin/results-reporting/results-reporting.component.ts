import {Component, OnInit} from '@angular/core';
import {ActivitiesService} from "../../../services/activities.service";

@Component({
    selector: 'app-results-reporting',
    templateUrl: './results-reporting.component.html',
    styleUrls: ['./results-reporting.component.scss']
})
export class ResultsReportingComponent implements OnInit {
      unames: any;
      activityNumber = 'Select';
      student;


      senderAddMarks: Number = 0;
        recvAddMarks: Number = 0;
      contentBodyMarks: Number = 0;
      compcloseMarks: Number = 0;
      letterRemarks;
      totmarksLetter: Number = 0;

    jsonActivity1;
    jsonActivity2;


    silentsMarks = 0;
    silentlMarks = 0;
    silentkMarks = 0;
    silentNMarks = 0;
    silentTMarks = 0;
    totactivity2 = 0;
    activity2Remarks;

    constructor(private activity: ActivitiesService) {
    }

    ngOnInit() {

        this.activity.getAllUserName().subscribe(
            res => {
                this.unames = res.json();
            }
        )


    }

    selected() {
        console.log(this.activityNumber);
        console.log(this.student);

        this.activity.retriveActivity1(this.student,'activity'+   this.activityNumber ).subscribe(
            res=>{
                 this.jsonActivity1=(res.json()[0]);
               // console.log(JSON.stringify(this.jsonActivity1));
            }
        )

    }


    saveMarksOfActivity1() {
        this.activity.saveMarksForActivity1(this.student, this.totmarksLetter, this.letterRemarks).subscribe(res => {

        })

    }

    saveMarksOfActivity2() {
        this.activity.saveMarksForActivity2(this.student, this.totactivity2, this.activity2Remarks).subscribe(res => {

        })
    }

    letterChange() {
        this.totmarksLetter = +this.senderAddMarks + +this.compcloseMarks + +this.recvAddMarks + +this.contentBodyMarks;
    }


    activity2Change() {
        this.totactivity2 = +this.silentsMarks + +this.silentkMarks + +this.silentlMarks + +this.silentTMarks + +this.silentNMarks;
    }

}
