import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(public router: Router) {
    router.events.subscribe((url: any) => {
      //  console.log(router.url);

      this.changeState(router.url);
    });
  }

  changeState(url): any {

  }

  toast(){

  }

}
