import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
 
@Injectable()
export class StudentAuth implements CanActivate {
 
    constructor(private router: Router) { }
 
    canActivate() {
        if (localStorage.getItem('student')) {
            // logged in so return true
        //    this.router.navigate(['/activity-page']);
            return true;
        }
 
        // not logged in so redirect to login page
        this.router.navigate(['/index']);
        return false;
    }
}