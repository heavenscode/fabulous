/**
 * Created by viroj on 6/4/2018.
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Property} from "./property";

@Injectable()
export class EvaluvateService {
    webHost="http://fabulous-rusl.000webhostapp.com/";
    local="http://localhost/";

    property = new Property();


    constructor(private httpClient: HttpClient) {
        this.webHost=this.property.getEnv();
    }

    public getActivity1(): Observable<any>{
        //Fetches the product-list file with HttpClient get method.

        return this.httpClient.get(this.webHost+'php/answer/reporting/achiever_activity1.php');
    }
    public getActivity2(): Observable<any> {
        //Fetches the product-list file with HttpClient get method.
        return this.httpClient.get(this.webHost+'php/answer/reporting/achiever_activity2.php');
    }
}