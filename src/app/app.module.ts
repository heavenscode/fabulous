import {BrowserModule} from '@angular/platform-browser';
import {NgModule, Input} from '@angular/core';


import {RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {NavbarComponent} from './shared/navbar/navbar.component';

import {routes} from './app-routing.module';
import {LandingComponent} from './pages/landing/landing.component';
import {FeaturesComponent} from './pages/features/features.component';
import {AboutComponent} from './pages/about/about.component';
import {PricingComponent} from './pages/pricing/pricing.component';
import {LessonRowComponent} from './pages/landing/lesson-row/lesson-row.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminComponent} from './pages/admin/admin.component';
import {CreateAccComponent} from './pages/admin/create-acc/create-acc.component';
import {HttpClientModule} from '@angular/common/http';  // replaces previous Http service
import {HttpHeaders} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import {RegisterStudent} from './services/registerstudent.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {ToastrModule} from 'ngx-toastr';
import {ResultsReportingComponent} from './pages/admin/results-reporting/results-reporting.component';
import {AuthenticationService} from './services/authentication.service';

import {StudentAuth} from './_guards/stdauth.guard';
import {ActivityPageComponent} from './pages/activity-page/activity-page.component';
import {AdminAuth} from './_guards/admin.authenticate';
import {LessonComponent} from './pages/activity-page/lesson/lesson.component';
import {McqComponent} from './pages/activity-page/mcq/mcq.component';

import {Lesson1Component} from './pages/activity-page/lesson1/lesson1.component';
import {Lesson2Component} from './pages/activity-page/lesson2/lesson2.component';
import {Lesson3Component} from './pages/activity-page/lesson3/lesson3.component';
import {Lesson4Component} from './pages/activity-page/lesson4/lesson4.component';
import {Lesson5Component} from './pages/activity-page/lesson5/lesson5.component';
import {Lesson6Component} from './pages/activity-page/lesson6/lesson6.component';
import {Lesson7Component} from './pages/activity-page/lesson7/lesson7.component';
import {Lesson8Component} from './pages/activity-page/lesson8/lesson8.component';
import {Lesson9Component} from './pages/activity-page/lesson9/lesson9.component';
import {Lesson10Component} from './pages/activity-page/lesson10/lesson10.component';

import {ActivitiesService} from './services/activities.service';

import {Letter} from './pages/activity-page/lesson1/Letter';
import { EvaluvateComponent } from './pages/admin/evaluvate/evaluvate.component';
import {EvaluvateService } from './services/evaluvate.service';

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        LandingComponent,
        FeaturesComponent,
        AboutComponent,
        PricingComponent,
        LessonRowComponent,
        AdminComponent,
        CreateAccComponent,
        ResultsReportingComponent,
        ActivityPageComponent,
        LessonComponent,
        McqComponent,

        Lesson1Component,
        Lesson2Component,
        Lesson3Component,
        Lesson4Component,
        Lesson5Component,
        Lesson6Component,
        Lesson7Component,
        Lesson8Component,
        Lesson9Component,
        Lesson10Component,
        EvaluvateComponent,

    ],
    imports: [
        BrowserModule, RouterModule,
        routes, FormsModule, FormsModule,
        ReactiveFormsModule, HttpClientModule, HttpModule,
        BrowserAnimationsModule, // required animations module
        ToastrModule.forRoot() // ToastrModule added

    ],
    providers: [RegisterStudent,
        AuthenticationService
        , StudentAuth,
        AdminAuth,
        ActivitiesService,
        EvaluvateService


    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
